<?php

/**
 * FileWatcher Daemon/Application
 * 
 * Watches given paths for any modifications and emails
 * the given users information about those modifications.
 * 
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 * @author Kyle Keiper <kkeiper1103@gmail.com>
 * @package FileWatcher
 */
namespace FileWatcher;

/**
 * Application Class
 * 
 * This is the main class of the FileWatch Daemon. Handles 
 * Starting Inotify and adding watches and handling events.
 */
class Application {
	
	/**
	 * @var string VERSION String Containing Version of FileWatcher
	 * @access public
	 */
	const VERSION = "0.2.0";
	
	/**
	 * @var resource $_fd Resource return by *inotify_init*
	 * @access protected
	 */
	protected $_fd = null;
	
	/**
	 * @var array $_paths Array of paths to be watched
	 * @access protected
	 */
	protected $_paths = array();
	
	/**
	 * @var array $_watches Array of paths that are currently being watched
	 * @access protected
	 */
	protected $_watches = array();
	
	/**
	 * @var int $_flags Bit Mask of Events to listen for
	 * @access protected
	 */
	protected $_flags = null;
	
	/**
	 * @var array $_allFlags Array of all Bit Masks possible. This is used to get String Equivalent of 
	 * the triggering event.
	 * @access protected
	 */
	protected $_allFlags = array(
		IN_ACCESS => "IN_ACCESS", 
		IN_MODIFY => "IN_MODIFY", 
		IN_ATTRIB => "IN_ATTRIB", 
		IN_CLOSE_WRITE => "IN_CLOSE_WRITE", 
		IN_CLOSE_NOWRITE => "IN_CLOSE_NOWRITE",
		IN_OPEN => "IN_OPEN", 
		IN_MOVED_TO => "IN_MOVED_TO", 
		IN_MOVED_FROM => "IN_MOVED_FROM", 
		IN_CREATE => "IN_CREATE", 
		IN_DELETE => "IN_DELETE", 
		IN_DELETE_SELF => "IN_DELETE_SELF",
		IN_MOVE_SELF => "IN_MOVE_SELF", 
		IN_CLOSE => "IN_CLOSE", 
		IN_MOVE => "IN_MOVE", 
		IN_ALL_EVENTS => "IN_ALL_EVENTS", 
		IN_UNMOUNT => "IN_UNMOUNT", 
		IN_Q_OVERFLOW => "IN_Q_OVERFLOW",
		IN_IGNORED => "IN_IGNORED", 
		IN_ISDIR => "IN_ISDIR", 
		IN_ONLYDIR => "IN_ONLYDIR", 
		IN_DONT_FOLLOW => "IN_DONT_FOLLOW", 
		IN_MASK_ADD => "IN_MASK_ADD", 
		IN_ONESHOT => "IN_ONESHOT"
	);
	
	/**
	 * Flag to recursively watch directories
	 * 
	 * @var boolean $_recursive
	 */
	protected $_recursive = false;
	
	/**
	 * Class constructor
	 * 
	 * @return Application
	 */
	public function __construct()
	{
		if( extension_loaded("inotify") === false )
			throw new \Exception("PECL \"inotify\" Extension Required! Please Install inotify.");
			
		// start inotify
		$this->_fd = inotify_init();
		
		// maybe do some error checking to ensure valid flags?
		$this->_flags = Config::get("flags");
		
		$this->_recursive = Config::get("recursive");
		
		// sort array by keys. mostly for debugging
		ksort($this->_allFlags);
		
		$this->setPaths( Config::get("paths") );
		
		// we don't want inotify to block.
		// readStreams, writeStreams, exceptStreams, timeout
		$read = array($this->_fd);
		$write = null;
		$except = null;
		stream_select($read, $write, $except, 0);
		// signature: stream_select(&$read, &$write, &$except)
		
		return $this;
	}
	
	/**
	 * Class Destructor
	 * 
	 * unsets all inotify file descriptors
	 * 
	 * @return void
	 */
	public function __destruct()
	{
		foreach($this->getPaths() as $path)
			$this->_removeWatch($path);
		
		if( ! is_null($this->_fd) )
			fclose($this->_fd);
	}
	
	/**
	 * Set the paths that should be watched by the Application
	 * 
	 * @param array $paths Array of paths to watch
	 * @return Application
	 * @access public
	 */
	public function setPaths( array $paths )
	{
		// unset all watches
		foreach($this->getPaths() as $path)
			$this->removePath($path);
	    
        foreach($paths as $path)
            $this->addPath($path);
		
		return $this;
	}
	
	/**
	 * Get all paths currently being watched
	 * 
	 * @return array
	 * @access public
	 */
	public function getPaths()
	{
		return $this->_paths;
	}
	
	/**
	 * Add a single path to the current paths
	 * 
	 * @param string $path Path to add inotify watch
	 * @return Application
	 * @access public
	 */
	public function addPath( $path )
	{
		if( !empty($path) && ! in_array($path, $this->getPaths() ) )
		{
		    
            
            $this->_paths[] = $path;
		    $this->_addWatch($path);
		}
		
		return $this;
	}
	
	/**
	 * Remove a path from the current paths being watched
	 * 
	 * @param string $path Path to remove from watches
	 * @return Application
	 * @access public
	 */
	public function removePath( $path )
	{
		if( ($index = array_search($path, $this->getPaths() )) !== false )
		{
			$this->_removeWatch($path);
			
			unset($this->_paths[$index]);
		}
		
		return $this;
	}
	
	/**
	 * "Entry Point" of Application
	 * 
	 * @return void
	 * @access public
	 */
	public function run()
	{
		$done = false;
        
		while( ! $done )
		{
			// since we're a daemon, cycle only once 
			// per second so we don't kill the CPU
			sleep(1);
			
			// if there are events to process
			if($evts = inotify_read($this->_fd))
			{
				// loop through them and process each one
				foreach($evts as $evtStruct)
				{
					$event = new Event($this, $evtStruct);
					
					$event->process();
				}
			}
			
			// find some way to kill the loop
			// if( some condition )
			// 		$done = true;
		}
	}
	
	/**
	 * Internal function to call inotify_add_watch
	 * 
	 * @param string $path Path that inotify will watch
	 * @return void
	 * @access protected
	 */	
	protected function _addWatch($path)
	{
		if( empty( $this->_watches[$path] ) )
		{
			$this->_watches[$path] = inotify_add_watch($this->_fd, $path, $this->_flags);
		}
		else
		{
			$this->_removeWatch($path);
			$this->_addWatch($path);
		}
	}
	
	/**
	 * Internal function to call inotify_rm_watch
	 * 
	 * @param string $path Path to remove via inotify_rm_watch
	 * @return void
	 * @access protected
	 */
	protected function _removeWatch($path)
	{
		if( array_key_exists($path, $this->_watches) ){
			
			inotify_rm_watch( $this->_fd, $this->_watches[$path] );
			
			unset($this->_watches[$path]);
		}
			
	}
	
	/**
	 * Gets the specified Flags
	 * 
	 * @param int $numerical Integer Value of flags to retreive
	 * @return array
	 * @access public
	 */
	public function getFlags( $numerical )
	{
		$tmp = array();
		
		// loop through all of the possible flags
		foreach( $this->_allFlags as $flag => $value )
		{
			// I don't quite understand how this works, but
			// if the current flag is set in $numerical,
			// then...
			if( $numerical & $flag )
			{
				// add that index's value to the return array
				$tmp[] = $value;
			}
		}
		
		// return array of all flags found
		return $tmp;
	}
	
    /**
     * Gets the path related to the given watch descriptor
     * 
     * @param resource $watchDescriptor Watch Descriptor Resource
     * @return string
     * @access public
     */
    public function getPathForWatchDescriptor($watchDescriptor)
    {
        return array_search($watchDescriptor, $this->_watches);
    }
    
}
