<?php

/**
 * FileWatcher Daemon/Application
 * 
 * Watches given paths for any modifications and emails
 * the given users information about those modifications.
 * 
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 * @author Kyle Keiper <kkeiper1103@gmail.com>
 * @package FileWatcher
 */
namespace FileWatcher;

/**
 * Config Class
 * 
 * Singleton Class used to define configurations for the application
 * 
 * @since 0.2.0
 */
class Config {
    
    /**
     * @var Config $_instance Protected variable to hold the instance of Config
     * 
     * @access protected
     */
    static protected $_instance = null;
    
    /**
     * @var array $_config Configuration array holding the array defined in {ROOT}/config.php
     * 
     * @access protected
     */
    protected $_config = array();
    
    /**
     * Method to get the instantiated Config class
     * 
     * @access public
     * @return Config
     */
    static public function Instance()
    {
        if( is_null(self::$_instance) )
            self::$_instance = new Config();
        
        return self::$_instance;
    }
    
    /**
     * Class Constructor. Loads the config file into memory
     * 
     * @access private
     * @return Config
     */
    private function __construct()
    {
        $this->_config = require_once( implode(DIRECTORY_SEPARATOR, array(ROOT, "config.php")) );
        
        return $this;
    }
    
    /**
     * Magic method to allow Config::get($name)
     * 
     * @access public
     * @return Method
     */
    static public function __callStatic( $name, $args )
    {
        return self::Instance()->{$name}($args);
    }
    
    /**
     * Retrieves an index from the configuration array
     * 
     * @access public
     * @return TypeOf $_config[$name]
     */
    public function get( $name )
    {
        return (isset( self::Instance()->_config[$name])) ? self::Instance()->_config[$name] : null;
    }
    
}
