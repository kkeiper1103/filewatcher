<?php

// config file
return array(
    
    // Paths to be watched by FileWatcher
    'paths' => array(
        "/path/to/watch",
        "/another/path/to/watch"
    ),
    
    // events that need watched for,
    // bitmasks chained together
    'flags' => IN_DELETE | IN_CREATE | IN_CLOSE_WRITE | IN_MOVE | IN_MOVE_SELF | IN_DELETE_SELF,
    
    // whether to watch the directories recursively
    'recursive' => true
    
);
